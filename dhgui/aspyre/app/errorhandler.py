from flask import render_template, url_for

from .app import app

@app.errorhandler(404)
def cant_find_page(error):
    """Redirect to 404.html"""
    return render_template("error/404.html", title="Aspyre | Error 404", aspyre_version = app.config['ASPYRE_VERSION']), 404

@app.errorhandler(500)
def server_unavailable(error):
    """Redirect to 500.html"""
    return render_template("error/500.html", title="Aspyre | Error 500", aspyre_version = app.config['ASPYRE_VERSION'])