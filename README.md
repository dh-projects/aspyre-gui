# dh-projects-gui

A GUI and API for tools developped in [DH-project](https://gitlab.inria.fr/dh-projects)

## Aspyre

Aspyre takes ALTO XML files exported from Transkribus and returns a slightly modified ALTO XML file that can be uploaded onto eScriptorium to import segmentation or transcription. For more details, see [the diagram describing Aspyre](https://gitlab.inria.fr/dh-projects/aspyre-gt#diagram) 

### Demo server
:arrow_right: https://aspyre-gui.herokuapp.com/

- Note that a script can be executed manually from Heroku's user interface in order to remove folders older than 7 days in `./dhgui/aspyre/uploads`
